package cache

import (
	"os"
	"testing"
)

const (
	cacheFile = "/tmp/test.json"
)

func TestLoadMissingCache(t *testing.T) {
	c, err := NewCache("doesnotexist")
	if err != nil {
		t.Errorf("load missing cache: %v", err)
	} else if c == nil {
		t.Error("nil return")
	}
}

func TestGetNodeMissing(t *testing.T) {
	c, err := NewCache("doesnotexist")
	if err != nil {
		t.Errorf("load cache: %s", err)
	} else if c == nil {
		t.Error("returned cache was nil")
	}
	n := c.GetNode("doesnotexist")
	if n != nil {
		t.Error("expected nil return from get node")
	}
}

func TestGetNodePresent(t *testing.T) {
	c, err := NewCache("doesnotexist")
	if err != nil {
		t.Errorf("load cache: %s", err)
	} else if c == nil {
		t.Error("returned cache was nil")
	}
	c.EnsureNode("ifr0")
	n := c.GetNode("ifr0")
	if n == nil {
		t.Error("expected non-nil return from get node")
	}
}

func TestIPisAllocated(t *testing.T) {
	c, err := NewCache("doesnotexist")
	if err != nil {
		t.Errorf("load cache: %s", err)
	} else if c == nil {
		t.Error("returned cache was nil")
	}
	c.EnsureNode("ifr0").SetIP("10.0.0.1").SetIPMI("10.0.0.2")
	n := c.GetNode("ifr0")
	if n == nil {
		t.Error("expected non-nil return from get node")
	}

	if !c.IsAllocated("10.0.0.1") {
		t.Error("expected cache to return IP is allocated")
	}
	if !c.IsAllocated("10.0.0.2") {
		t.Error("expected cache to return IP is allocated")
	}

	if c.IsAllocated("10.0.0.3") {
		t.Error("expected cache to return IP is unallocated")
	}
}

func TestSaveCache(t *testing.T) {
	os.Remove(cacheFile)

	c, err := NewCache(cacheFile)

	c.EnsureNode("ifr0").SetIP("10.0.0.1")
	c.EnsureNode("emu0").SetIP("10.0.0.2").AddBGP("ifabric", 42, "10.99.0.1")

	err = c.Commit()
	if err != nil {
		t.Errorf("save /tmp/test.json: %s", err)
	}
}

func TestLoadCache(t *testing.T) {
	c, err := NewCache(cacheFile)
	if err != nil {
		t.Errorf("load cache: %s", err)
	} else if c == nil {
		t.Error("returned cache was nil")
	}
}
