package cache

import (
	"encoding/json"
	"fmt"
	"os"
)

// Cache of node attributes that we want to remain persistent across runs of facility-install

type BGP struct {
	ASN uint32 `json:"asn"` // ASN for underlay
	IP  string `json:"ip"`  // IP address for the underlay
}

type Node struct {
	IP     string          `json:"ip,omitempty"`     // IP address for management network
	IPMI   string          `json:"ipmi,omitempty"`   // IP address for the IPMI controller
	BGP    map[string]*BGP `json:"bgp,omitempty"`    // vrf -> BGP config
	HwAddr string          `json:"hwaddr,omitempty"` // hardware address assigned to the macvlan interface on infrapod servers
}

type CacheData struct {
	Version uint32           `json:"version"` // version 0
	Nodes   map[string]*Node `json:"nodes"`
}

type Cache struct {
	file string
	data CacheData
}

// find the cache entry for the specified resource
// returns nil if the node is not found
func (c *Cache) GetNode(s string) *Node {
	if n, ok := c.data.Nodes[s]; ok {
		return n
	}
	return nil
}

func (c *Cache) HasNode(s string) bool {
	_, ok := c.data.Nodes[s]
	return ok
}

func (c *Cache) Commit() error {
	if c.file == "" {
		return fmt.Errorf("no cache file specified")
	}
	return c.Save(c.file)
}

// write the cache file to disk
func (c *Cache) Save(path string) error {
	b, err := json.MarshalIndent(c.data, "", "  ")
	if err != nil {
		return err
	}
	return os.WriteFile(path, b, 0644)
}

// Create a new cache from a file on disk.
func NewCache(path string) (*Cache, error) {
	c := &Cache{file: path}
	c.data.Nodes = make(map[string]*Node, 0)

	b, err := os.ReadFile(path)
	if err == nil {
		err = json.Unmarshal(b, &c.data)
		if err != nil {
			return nil, fmt.Errorf("unmarshal json: %v", err)
		}
	} else if !os.IsNotExist(err) {
		return nil, fmt.Errorf("read file: %s: %w", path, err)
	}

	return c, nil
}

// Determine if the given IP address has been allocated to a node present in the cache
func (c *Cache) IsAllocated(ip string) bool {
	for _, n := range c.data.Nodes {
		if n.IP == ip || n.IPMI == ip {
			return true
		}
	}
	return false
}

// Determine if the specified ASN is already allocated for the vrf
func (c *Cache) IsAllocatedASN(vrf string, asn uint32) bool {
	for _, n := range c.data.Nodes {
		if b, ok := n.BGP[vrf]; ok {
			if b.ASN == asn {
				return true
			}
		}
	}
	return false
}

func (c *Cache) NewNode(name string) *Node {
	n := &Node{}
	n.BGP = make(map[string]*BGP, 0)
	c.data.Nodes[name] = n
	return n
}

// returns the cache entry for the specified node, or creates a new cache entry if none exists
func (c *Cache) EnsureNode(s string) *Node {
	if n, ok := c.data.Nodes[s]; ok {
		return n
	}
	return c.NewNode(s)
}

func (n *Node) SetIP(ip string) *Node {
	n.IP = ip
	return n
}

func (n *Node) GetIP() string {
	if n == nil {
		return ""
	}
	return n.IP
}

func (n *Node) GetIPMI() string {
	if n == nil {
		return ""
	}
	return n.IPMI
}

func (n *Node) SetIPMI(ip string) *Node {
	n.IPMI = ip
	return n
}

func (n *Node) AddBGP(vrf string, asn uint32, address string) *Node {
	if n == nil {
		return nil
	}
	n.BGP[vrf] = &BGP{
		ASN: asn,
		IP:  address,
	}
	return n
}
